
package relevel;

public class SubsetProblem {

    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5, 6};
        int subarr_len = 3;
        int len = arr.length;
        int data[] = new int[subarr_len];
        partition(arr, len, subarr_len, 0, data, 0);
    }

    static void partition(int arr[], int len, int subarr_len, int index, int data[], int i) {

        if (index == subarr_len) {
            int temp = 0;
            for (int j = 0; j < subarr_len; j++) {
                temp = temp + data[j];
                if (temp == 9) {
                    for (int k = 0; k < subarr_len; k++) {

                        System.out.print(data[k] + " ");
                    }
                }
            }
            System.out.println("");

            return;
        }

        if (i >= len) {
            return;
        }

        data[index] = arr[i];
        partition(arr, len, subarr_len, index + 1, data, i + 1);
        partition(arr, len, subarr_len, index, data, i + 1);

    }
}
